package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
        	System.out.println("Let's play round " + roundCounter);
        	System.out.println("Your choice (Rock/Paper/Scissors)?");
        	String humanChoice = sc.nextLine();
            while (!rpsChoices.contains(humanChoice)) {
            	System.out.println("I do not understand " + humanChoice + ". Could you try again?");
            	humanChoice = sc.nextLine();
            }
        	String computerChoice = rpsChoices.get((int) (Math.random() * 3));
        	System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + "." + " " + (computerChoice.equals(humanChoice) ? "It's a tie!" : (computerChoice.equals("rock") && humanChoice.equals("scissors") || computerChoice.equals("paper") && humanChoice.equals("rock") || computerChoice.equals("scissors") && humanChoice.equals("paper") ? "Computer wins!" : "Human wins!")));;
        	if (humanChoice.equals(computerChoice)) {
        		
        	} else if (humanChoice.equals("rock") && computerChoice.equals("scissors") || humanChoice.equals("paper") && computerChoice.equals("rock") || humanChoice.equals("scissors") && computerChoice.equals("paper")) {
        		humanScore++;
        	} else {
        		computerScore++;
        	}

        	roundCounter++;
        	System.out.println("Score: human " + humanScore + ", computer " + computerScore);
        	System.out.println("Do you wish to continue playing? (y/n)?");
        	String answer = sc.nextLine();
        	if (answer.equals("n")) {
                System.out.println("Bye bye :)");
        		break;
        	} else  {
        		continue;
        	}
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
